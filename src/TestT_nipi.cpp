#include "TestT_nipi.h"

int main() {
	//std::vector<double> t1 = { 1,2,3,4 };
	//std::vector<double> t2 = { 1, 2, 3, 4 };
	//t2 = t2 * 2;
	//std::cout << t1 << "\n";
	//std::cout << t2 << "\n";
	//t2 = mult(t1,t2);
	//std::cout << t2 << "\n";

	//t2 = t2 / 2;
	//std::cout << t2 << "\n";

	std::vector<double> t = linspace(0., 1., 200);
	std::vector<double> s(t.size());
	for (int i = 0; i < s.size(); i++)
	{
		s[i] = cos(11. * 2. * M_PI * t[i] * t[i]) + 6. * t[i] * t[i];
	}
	

	double sd = 0.2;
	int iter_max_sifting = 1000;
	int iter_max_decomposition = 10;
	SpectralDecompostion<double, int> SpctDcmps(t, sd);
	//---------------------------------EMD--------------------------------------------
	SpctDcmps.EMD(s, iter_max_decomposition, iter_max_sifting);


	std::vector<std::vector<double>> imfsEMD = SpctDcmps.getImfs();

	sciplot::Plot2D plotEMD;

	plotEMD.xlabel("t");
	plotEMD.ylabel("y");

	plotEMD.drawCurve(t, s).label("signal");
	for (int i = 0; i < imfsEMD.size(); i++)
	{
		plotEMD.drawCurve(t, imfsEMD[i]).label("imf EMD" + std::to_string(i + 1));
	}
	sciplot::Figure figEMD = { {plotEMD}, };
	figEMD.title("test2");
	figEMD.palette("dark2");

	// Create canvas to hold figure
	sciplot::Canvas canvasEMD = { {figEMD} };
	// Set canvas output size
	canvasEMD.size(1020, 400);

	// Show the plot in a pop-up window
	canvasEMD.show();

	// Save the figure to a PDF file
	canvasEMD.save("canvasEMd.pdf");

	SpctDcmps.SaveFileImf("D:/dev/TestT_nipi/data/MyRES_EMD.csv");

	//---------------------------------EEMD--------------------------------------------
	int nEnsemble = 10;
	double mean = 0.;
	double stddev = 1.;
	double epsilon = 0.1;
	SpctDcmps.EEMD(s, nEnsemble, mean, stddev, epsilon);


	std::vector<std::vector<double>> imfsEEMD = SpctDcmps.getImfs();

	sciplot::Plot2D plotEEMD;

	plotEEMD.xlabel("t");
	plotEEMD.ylabel("y");


	plotEEMD.drawCurve(t, s).label("signal");
	for (int i = 0; i < imfsEEMD.size(); i++)
	{
		plotEEMD.drawCurve(t, imfsEEMD[i]).label("imf EEMD" + std::to_string(i + 1));
	}
	sciplot::Figure figEEMD = { {plotEEMD}, };
	figEEMD.title("test2");
	figEEMD.palette("dark2");

	// Create canvas to hold figure
	sciplot::Canvas canvasEEMD = { {figEEMD} };
	// Set canvas output size
	canvasEEMD.size(1020, 400);

	// Show the plot in a pop-up window
	canvasEEMD.show();

	// Save the figure to a PDF file
	canvasEEMD.save("canvasEEMD.pdf");

	SpctDcmps.SaveFileImf("D:/dev/TestT_nipi/data/MyRES_EEMD.csv");

	//---------------------------------CEEMD--------------------------------------------
	SpctDcmps.CEEMD(s, iter_max_decomposition, nEnsemble, mean, stddev, epsilon);


	std::vector<std::vector<double>> imfsCEEMD = SpctDcmps.getImfs();

	sciplot::Plot2D plotCEEMD;

	plotCEEMD.xlabel("t");
	plotCEEMD.ylabel("y");


	plotCEEMD.drawCurve(t, s).label("signal");
	for (int i = 0; i < imfsCEEMD.size(); i++)
	{
		plotCEEMD.drawCurve(t, imfsCEEMD[i]).label("imf CEEMD" + std::to_string(i + 1));
	}
	sciplot::Figure figCEEMD = { {plotCEEMD}, };
	figCEEMD.title("test2");
	figCEEMD.palette("dark2");

	// Create canvas to hold figure
	sciplot::Canvas canvasCEEMD = { {figCEEMD} };
	// Set canvas output size
	canvasCEEMD.size(1020, 400);

	// Show the plot in a pop-up window
	canvasCEEMD.show();

	// Save the figure to a PDF file
	canvasCEEMD.save("canvasCEEMD.pdf");

	SpctDcmps.SaveFileImf("D:/dev/TestT_nipi/data/MyRES_CEEMD.csv");
	return 1;
}