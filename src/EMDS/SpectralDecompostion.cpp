#include <EMDS/SpectralDecompostion.h>
template<typename value_t, typename ind_t>
SpectralDecompostion<value_t, ind_t>::SpectralDecompostion(std::vector<value_t>& t, value_t sd)
{
	_t = t;
	_sd = sd;
}
//----------------------------------------------------------------------------------------------------------------------------------
template<typename value_t, typename ind_t>
void SpectralDecompostion<value_t, ind_t>::ExtractIMF(std::vector<value_t>& residue, std::vector<value_t>& imf, ind_t iter_max_sifting)
{
	ind_t iter_sifting = 0;
	value_t delta = 0;
	std::vector<value_t> residue_tmp = residue;
	for (;;) {
		// ������� ����������. _extrm_upper && _extrm_lower
		Extremes(residue_tmp);

		//std::cout << "Sifting I =" << iter_sifting + 1 << "\n";
		//std::cout << "count extrm " << _extrm_count << "\n";
		//std::cout << "t max extrm " << _t_extrm_upper << "\n";
		//std::cout << "max extrm " << _extrm_upper << "\n";
		//std::cout << "t min extrm " << _t_extrm_lower << "\n";
		//std::cout << "min extrm " << _extrm_lower << "\n";

		if (_t_extrm_upper.size() < 3 || _t_extrm_lower.size() < 3) {
			imf = residue_tmp;
			residue = residue - imf;
			return;
		}
		
		
		// ������������
		tk::spline sp_u(_t_extrm_upper, _extrm_upper);
		_envelope_upper.clear();
		for (const auto& tt : _t)
		{
			_envelope_upper.push_back(sp_u(tt));
		}
		tk::spline sp_l(_t_extrm_lower, _extrm_lower);
		_envelope_lower.clear();
		for (const auto& tt : _t)
		{
			_envelope_lower.push_back(sp_l(tt));
		}

		std::vector<value_t> envelope_mean = (_envelope_upper + _envelope_lower) / static_cast<value_t>(2);

		//sciplot::Plot2D plot;
		//plot.xlabel("t");
		//plot.ylabel("y");


		//plot.drawPoints(_t_extrm_upper, _extrm_upper).label("extrmUpper");
		//plot.drawPoints(_t_extrm_lower, _extrm_lower).label("extrmLower");
		//plot.drawCurve(_t, _envelope_lower).label("Lower");
		//plot.drawCurve(_t, _envelope_upper).label("Upper");
		//plot.drawCurve(_t, envelope_mean).label("envelopeMean");
		//plot.drawCurve(_t, residue_tmp).label("residueTmp");


		//sciplot::Figure fig = { {plot}, };
		//fig.title("test");
		//fig.palette("dark2");

		//// Create canvas to hold figure
		//sciplot::Canvas canvas = { {fig} };
		//// Set canvas output size
		//canvas.size(1020, 400);

		//// Show the plot in a pop-up window
		//canvas.show();

		residue_tmp = residue_tmp - envelope_mean;

		delta = sum(mult(envelope_mean, envelope_mean)) / sum(mult(residue_tmp, residue_tmp));

		//std::cout << "DELTA: " << delta << "\n";
		if (delta < _sd || iter_sifting >= iter_max_sifting)
		{
			imf = residue_tmp;
			residue = residue - imf;
			return;
		}
		
		iter_sifting++;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
template<typename value_t, typename ind_t>
void SpectralDecompostion<value_t, ind_t>::Extremes(std::vector<value_t>& input)
{
	_t_extrm_upper.clear();
	_t_extrm_lower.clear();

	_extrm_upper.clear();
	_extrm_lower.clear();

	if (input[0] > input[1]) {
		_t_extrm_upper.push_back(_t[0]);
		_extrm_upper.push_back(input[0]);
	}

	else if (input[0] < input[1])
	{
		_t_extrm_lower.push_back(_t[0]);
		_extrm_lower.push_back(input[0]);
	}

	size_t n = input.size();

	for (int i = 1; i < n - 1; i++)
	{
		if ((input[i - 1] > input[i]) && (input[i] < input[i + 1])) {
			_t_extrm_lower.push_back(_t[i]);
			_extrm_lower.push_back(input[i]);
		}

		else if ((input[i - 1] < input[i]) && (input[i] > input[i + 1])) {
			_t_extrm_upper.push_back(_t[i]);
			_extrm_upper.push_back(input[i]);
		}
	}
	if (input[n - 1] > input[n - 2]) {
		_t_extrm_upper.push_back(_t[n - 1]);
		_extrm_upper.push_back(input[n - 1]);
	}

	else if (input[n - 1] < input[n - 2]) {
		_t_extrm_lower.push_back(_t[n - 1]);
		_extrm_lower.push_back(input[n - 1]);
	}

	_extrm_count = static_cast<ind_t>(_t_extrm_upper.size() + _t_extrm_lower.size());
}
//----------------------------------------------------------------------------------------------------------------------------------
template<typename value_t, typename ind_t>
void SpectralDecompostion<value_t, ind_t>::AddNoise(std::vector<value_t>& input, value_t mean, value_t stddev, value_t epsilon)
{
	auto dist = std::bind(std::normal_distribution<value_t>{mean, stddev}, std::mt19937(std::random_device{}()));

	for (auto& x : input) {
		x = x + epsilon * dist();
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
template<typename value_t, typename ind_t>
void SpectralDecompostion<value_t, ind_t>::EMD(std::vector<value_t>& signal, ind_t iter_max_decomposition, ind_t iter_max_sifting )
{
	_imf.clear();
	_residue = signal;
	ind_t j = 0;
	for (;;) {
		Extremes(_residue);
		if (_extrm_count > 4 && j < iter_max_decomposition) {
			_imf.resize(j + 1);
			_imf[j].resize(signal.size());
			//std::cout << "J = " << j << "\n";
			ExtractIMF(_residue, _imf[j], iter_max_sifting);
			j++;
		}
		else
		{
			break;
		}

	}
	//sciplot::Plot2D plot;
	//plot.xlabel("t");
	//plot.ylabel("y");
	//plot.drawCurve(_t, signal).label("signal");
	//std::vector<value_t> tmp(_t.size());
	//for (const auto& imf : _imf) {
	//	tmp = tmp + imf;
	//}
	//tmp = tmp + _residue;
	//plot.drawCurve(_t, tmp).label("signal=Imfs+res");
	//sciplot::Figure fig = { {plot}, };
	//fig.title("test");
	//fig.palette("dark2");
	//// Create canvas to hold figure
	//sciplot::Canvas canvas = { {fig} };
	//// Set canvas output size
	//canvas.size(1020, 400);
	//// Show the plot in a pop-up window
	//canvas.show();
}
//----------------------------------------------------------------------------------------------------------------------------------
template<typename value_t, typename ind_t>
void SpectralDecompostion<value_t, ind_t>::EEMD(std::vector<value_t>& signal, ind_t nEns, value_t mean, value_t stddev, value_t epsilon)
{
	_imf.clear();
	std::vector <std::vector<std::vector<value_t>>> imfs;
	std::vector<std::vector<value_t>> signals_noise(nEns);
	for (auto& signal_n : signals_noise) {
		signal_n = signal;
		AddNoise(signal_n, mean, stddev, epsilon);
	}
		
	for (ind_t i = 0; i < nEns; i++)
	{
		//std::cout << "----------------------------\nnEns = " << i << "\n";
		EMD(signals_noise[i], INT_MAX, INT_MAX);
		imfs.push_back(_imf);
	}

	size_t min = imfs[0].size();
	for (ind_t i = 1; i < nEns; i++)
	{
		if (imfs[i].size() < min)
		{
			min = imfs[i].size();
		}
	}

	_imf = imfs[0];
	for (ind_t i = 1; i < nEns; i++)
	{
		for (ind_t k = 0; k < min; k++)	
		{
			_imf[k] = _imf[k] + imfs[i][k];
			_imf[k] = _imf[k] / nEns;
		}
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
template<typename value_t, typename ind_t>
void SpectralDecompostion<value_t, ind_t>::CEEMD(std::vector<value_t>& signal, ind_t iter_max_decomposition, ind_t nEns, value_t mean, value_t stddev, value_t epsilon)
{
	_imf.clear();
	std::vector<std::vector<value_t>> imfs1;
	std::vector<std::vector<value_t>> imfs;
	std::vector<std::vector<value_t>> gauss_noise_emd(nEns);
	std::vector<std::vector<value_t>> signals_noise(nEns);
	std::vector<value_t> residue;


	for (auto& signal_n : signals_noise) {
		signal_n = signal;
		AddNoise(signal_n, mean, stddev, epsilon);
	}


	for (ind_t i = 0; i < nEns; i++)
	{
		EMD(signals_noise[i], 1, INT_MAX);
		imfs1.push_back(_imf[0]);
	}
	imfs.push_back(imfs1[0]);
	for (ind_t i = 1; i < nEns; i++)
	{
		imfs[0] = imfs[0] + imfs1[i];
		imfs[0] = imfs[0] / nEns;
	}


	//E_1(w^i(t))
	for (ind_t i = 0; i < nEns; i++)
	{
		gauss_noise_emd[i].resize(signal.size(), 0);
		AddNoise(gauss_noise_emd[i], mean, stddev);
		EMD(gauss_noise_emd[i], 1, INT_MAX);
		gauss_noise_emd[i] = _imf[0];
	}
	_imf.clear();


	residue = signal - imfs[0];
	std::vector<value_t> residue_tmp = residue;
	ind_t k = 1;
	for (;;) {
		Extremes(residue);
		if (_extrm_count > 4 && k < iter_max_decomposition) {
			imfs1.clear();
			for (ind_t i = 0; i < nEns; i++)
			{
				residue_tmp = residue + epsilon * gauss_noise_emd[i];
				EMD(residue_tmp, 1, INT_MAX);
				imfs1.push_back(_imf[0]);
			}
			imfs.push_back(imfs1[0]);
			for (ind_t i = 1; i < nEns; i++)
			{
				imfs[k] = imfs[k] + imfs1[i];
				imfs[k] = imfs[k] / nEns;
			}
			residue = residue - imfs[k];
			k++;
		}
		else {
			break;
		}
	}
	_imf = imfs;
}
//----------------------------------------------------------------------------------------------------------------------------------