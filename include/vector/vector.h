#pragma once
#include "Utils/Defines.h"
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <numeric>


template<typename value_t>
std::vector<value_t> operator+(const std::vector<value_t>& left, const std::vector<value_t>& right) {
#ifdef  NDEBUG
		if (left.size() != right.size())
		{
			std::stringstream err;
			err << "Size of vectors" << left.size() << " and " << right.size() << " arent equal" << ERR_INFO << "\n";
			throw std::runtime_error(err.str());
		}
#endif //  NDEBUG
		std::vector<value_t> res(left.size());
		std::transform(left.begin(), left.end(), right.begin(), res.begin(), std::plus<>());
		return res;
}

template<typename value_t, typename number_t>
std::vector<value_t> operator+(const std::vector<value_t>& left, number_t number) {
	std::vector<value_t> right(left.size(), static_cast<value_t>(number));
	return right + left;
}

template<typename value_t, typename number_t>
std::vector<value_t> operator+( number_t number, const std::vector<value_t>& right) {
	return right + number;
}


template<typename value_t, typename number_t>
std::vector<value_t> operator*(const std::vector<value_t>& left, number_t number) {
	std::vector<value_t> res;
	for (auto& a : left) {
		res.push_back(a * static_cast<value_t>(number));
	}
	return res;
}

template<typename value_t, typename number_t>
std::vector<value_t> operator*(number_t number, const std::vector<value_t>& right) {
	return right * number;
}


template<typename value_t, typename number_t>
std::vector<value_t> operator/(const std::vector<value_t>& left, number_t number) {
	std::vector<value_t> res;
	for (auto& a : left) {
		res.push_back(a * (static_cast<value_t>(1)/static_cast<value_t>(number)));
	}
	return res;
}


template<typename value_t>
std::ostream &operator << (std::ostream& os, const std::vector<value_t>& v) {
	//        os << "[";
	for (auto& a: v)
	{
		os << a << " ";
	};
	//        os << ']';
	os << "\n";
	return os;
}

template<typename value_t>
std::ostream& operator << (std::ostream& os, std::vector<value_t>& v) {
	//        os << "[";
	for (auto& a : v)
	{
		os << a << " ";
	};
	//        os << ']';
	os << "\n";
	return os;
}


template<typename value_t>
std::vector<value_t> operator-(const std::vector<value_t>& left, const std::vector<value_t>& right) {
	return left + (-1)* right;
}

template<typename value_t, typename number_t>
std::vector<value_t> operator-(const std::vector<value_t>& left, number_t number) {
	return left + (-1)*number;
}

template<typename value_t, typename number_t>
std::vector<value_t> operator-(number_t number, const std::vector<value_t>& right) {
	return right + (-1) * number;
}


template<typename value_t>
value_t sum(const std::vector<value_t>& v) {
	return std::reduce(v.begin(), v.end());
}

template<typename value_t>
std::vector<value_t> mult(const std::vector<value_t>& left, const std::vector<value_t>& right) {
#ifdef  NDEBUG
	if (left.size() != right.size())
	{
		std::stringstream err;
		err << "Size of vectors" << left.size() << " and " << right.size() << " arent equal" << ERR_INFO << "\n";
		throw std::runtime_error(err.str());
	}
#endif //  NDEBUG
	std::vector<value_t> res(left.size());
	std::transform(left.begin(), left.end(), right.begin(), res.begin(), std::multiplies<>());
	return res;
}

template<typename value_t>
static std::vector<value_t> linspace(value_t start_in, value_t end_in, int num_in)
{
	std::vector<value_t> linspaced;
	if (num_in == 0) { return linspaced; }
	if (num_in == 1)
	{
		linspaced.push_back(start_in);
		return linspaced;
	}

	value_t delta = (end_in - start_in) / (num_in - 1);

	for (int i = 0; i < num_in - 1; ++i)
	{
		linspaced.push_back(start_in + delta * i);
	}
	linspaced.push_back(end_in);
	return linspaced;
}