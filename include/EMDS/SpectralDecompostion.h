#pragma once
#include <vector/vector.h>
#include <Interpolation/spline.h>
#include <sciplot/sciplot.hpp>
#include <random>
template<typename value_t, typename ind_t>
class SpectralDecompostion
{
public:
	SpectralDecompostion(std::vector<value_t>& t, value_t sd);

	void EMD(std::vector<value_t>& signal, ind_t iter_max_decomposition, ind_t iter_max_sifting); /// ������������
	void EEMD(std::vector<value_t>& signal, ind_t nEns, value_t mean, value_t stddev, value_t epsilon); /// ������������
	void CEEMD(std::vector<value_t>& signal, ind_t iter_max_decomposition, ind_t nEns, value_t mean, value_t stddev, value_t epsilon); /// ������������

	void ExtractIMF(std::vector<value_t>& residue, std::vector<value_t>& imf, ind_t iter_max_sifting); /// ������� �����������

	void Extremes(std::vector<value_t>& input); /// ����� ��������� �����������

	void AddNoise(std::vector<value_t>& input, value_t mean, value_t stddev, value_t epsilon = 1.);

	std::vector<std::vector<value_t>> getImfs() { return _imf; }

	void SaveFileImf(std::string filename) {
		std::ofstream file(filename);
		for (const auto& imf : _imf) {
			for (int i = 0; i < imf.size() - 1; i++)
			{
				file << imf[i] << ";";
			}
			file << imf[imf.size() - 1] << "\n";
		}
		file.close();
	};
protected:
	
	ind_t _extrm_count; /// ���-�� ��������� �����������

	value_t _sd; /// ������� ��� �������� ��������� "�����������"

	std::vector<value_t> _t; /// 

	std::vector<value_t> _t_extrm_lower;
	std::vector<value_t> _t_extrm_upper;
	std::vector<value_t> _extrm_upper; /// �������� ������� � ��������� ������ ���������
	std::vector<value_t> _extrm_lower; /// �������� ������� � ��������� ������ ��������

	std::vector<value_t> _envelope_upper; /// ������� ���������
	std::vector<value_t> _envelope_lower; /// ������ ���������
	std::vector<value_t> _residue;  /// ������� �������� ������� � imf
	std::vector<std::vector<value_t>> _imf;/// imf ��� ����� ������������
public:
};
