# import numpy as np
# import pylab as plt
# from PyEMD import EMD

# # Define signal
# t = np.linspace(0, 1, 200)
# s = np.cos(11 * 2 * np.pi * t * t) + 6 * t * t

# # Execute EMD on signal
# IMF = EMD().emd(s, t)
# N = IMF.shape[0] + 1

# # Plot results
# plt.subplot(N, 1, 1)
# plt.plot(t, s, "r")
# plt.title("Input signal: $S(t)=cos(22\pi t^2) + 6t^2$")
# plt.xlabel("Time [s]")

# myemd = np.loadtxt("D:/dev/TestT_nipi/data/MyRES.csv",
#                  delimiter=";", dtype=np.float64)
# diff =  IMF[0] - myemd
# plt.subplot(N, 1, 2)
# plt.plot(t, diff)

# # for n, imf in enumerate(IMF):
# #     plt.subplot(N, 1, n + 2)
# #     plt.plot(t, imf, "g")
# #     plt.title("IMF " + str(n + 1))
# #     plt.xlabel("Time [s]")

# plt.tight_layout()
# plt.show()

from PyEMD import EEMD
import numpy as np
import pylab as plt

# Define signal
t = np.linspace(0, 1, 200)
s = np.cos(11 * 2 * np.pi * t * t) + 6 * t * t

# Assign EEMD to `eemd` variable
eemd = EEMD()


# Execute EEMD on S
eIMFs = eemd.eemd(s, t)
nIMFs = eIMFs.shape[0]

# Plot results
plt.figure(figsize=(12, 9))
plt.subplot(nIMFs + 1, 1, 1)
plt.plot(t, s, "r")

for n in range(nIMFs):
    plt.subplot(nIMFs + 1, 1, n + 2)
    plt.plot(t, eIMFs[n], "g")
    plt.ylabel("eIMF %i" % (n + 1))
    plt.locator_params(axis="y", nbins=5)

plt.xlabel("Time [s]")
plt.tight_layout()
plt.show()